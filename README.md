ws2ify
======

Convert Super Monkey Ball TXT stage configs to XML

## Usage

```sh
python3 run.py in-config in-obj out-xml [LINEAR/EASED]
```

Where LINEAR or EASED if your keyframe easing

Takes in-config and in-obj, and generates out-xml
