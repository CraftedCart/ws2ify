#!/usr/bin/env python3

from ws2ify.config_parser import ConfigParser
from ws2ify.xml_exporter import XmlExporter

import sys


def main():
    if len(sys.argv) < 5:
        print(
            "Not enough args\n"
            f"Usage: {sys.argv[0]} in-config in-obj out-xml [LINEAR/EASED]",
            file=sys.stderr
            )
        sys.exit(1)

    config_path = sys.argv[1]
    obj_path = sys.argv[2]
    xml_path = sys.argv[3]

    print(f":: Input config: {config_path}")
    print(f":: Input OBJ: {obj_path}")
    print(f":: Output XML: {xml_path}")

    print(":: Parsing config...")

    parser = ConfigParser(config_path)
    parser.parse_config()
    cfg = parser.package_config()

    print(":: Exporting XML...")

    exporter = XmlExporter(cfg, obj_path)
    exporter.export_to(xml_path, sys.argv[4])

    print(":: Done!")


if __name__ == "__main__":
    main()
