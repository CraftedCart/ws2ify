from ws2ify.objects import Goal, Bumper, Banana, Jamabar, Start, Fallout, AnimObj, Keyframe, Config, Bg

import sys
import os


class ConfigParser:
    def __init__(self, config_path: str):
        self.config_path = config_path
        self.goal_map = {}
        self.bumper_map = {}
        self.banana_map = {}
        self.jamabar_map = {}
        self.start_map = {}
        self.fallout_map = {}
        self.animobj_map = {}
        self.bg_map = {}

    def parse_config(self):
        with open(self.config_path, "r") as f:
            lines = f.readlines()

        for line in lines:
            vals = line.split()
            if len(vals) < 10:
                if len(vals) > 0:
                    print(f"!! Ignoring line \"{line[:-1]}\"", file=sys.stderr)
                continue

            if vals[0] == "goal":
                obj = self.goal_map.setdefault(vals[2], Goal())
                self.parse_goal_line(obj, vals)
            elif vals[0] == "bumper":
                obj = self.bumper_map.setdefault(vals[2], Bumper())
                self.parse_bumper_line(obj, vals)
            elif vals[0] == "banana":
                obj = self.banana_map.setdefault(vals[2], Banana())
                self.parse_banana_line(obj, vals)
            elif vals[0] == "jamabar":
                obj = self.jamabar_map.setdefault(vals[2], Jamabar())
                self.parse_jamabar_line(obj, vals)
            elif vals[0] == "start":
                obj = self.start_map.setdefault(vals[2], Start())
                self.parse_start_line(obj, vals)
            elif vals[0] == "fallout":
                obj = self.fallout_map.setdefault(vals[2], Fallout())
                self.parse_fallout_line(obj, vals)
            elif vals[0] == "animobj":
                obj = self.animobj_map.setdefault(vals[2], AnimObj())
                self.parse_animobj_line(obj, vals)
            elif vals[0] == "background":
                obj = self.bg_map.setdefault(vals[2], Bg())
                self.parse_bg_line(obj, vals)
            else:
                print(f"!! Ignoring line \"{line[:-1]}\"", file=sys.stderr)

        self.parse_animobjs()

    def parse_goal_line(self, obj, vals):
        if vals[5] == "pos":
            if vals[7] == "x":
                obj.pos[0] = float(vals[9])
            elif vals[7] == "y":
                obj.pos[1] = float(vals[9])
            elif vals[7] == "z":
                obj.pos[2] = float(vals[9])
        elif vals[5] == "rot":
            if vals[7] == "x":
                obj.rot[0] = float(vals[9])
            elif vals[7] == "y":
                obj.rot[1] = float(vals[9])
            elif vals[7] == "z":
                obj.rot[2] = float(vals[9])
        elif vals[5] == "type":
            obj.type = vals[9]

    def parse_bumper_line(self, obj, vals):
        if vals[5] == "pos":
            if vals[7] == "x":
                obj.pos[0] = float(vals[9])
            elif vals[7] == "y":
                obj.pos[1] = float(vals[9])
            elif vals[7] == "z":
                obj.pos[2] = float(vals[9])
        elif vals[5] == "rot":
            if vals[7] == "x":
                obj.rot[0] = float(vals[9])
            elif vals[7] == "y":
                obj.rot[1] = float(vals[9])
            elif vals[7] == "z":
                obj.rot[2] = float(vals[9])
        elif vals[5] == "scl":
            if vals[7] == "x":
                obj.scl[0] = float(vals[9])
            elif vals[7] == "y":
                obj.scl[1] = float(vals[9])
            elif vals[7] == "z":
                obj.scl[2] = float(vals[9])

    def parse_banana_line(self, obj, vals):
        if vals[5] == "pos":
            if vals[7] == "x":
                obj.pos[0] = float(vals[9])
            elif vals[7] == "y":
                obj.pos[1] = float(vals[9])
            elif vals[7] == "z":
                obj.pos[2] = float(vals[9])
        elif vals[5] == "type":
            obj.type = vals[9]

    parse_jamabar_line = parse_bumper_line

    def parse_start_line(self, obj, vals):
        if vals[5] == "pos":
            if vals[7] == "x":
                obj.pos[0] = float(vals[9])
            elif vals[7] == "y":
                obj.pos[1] = float(vals[9])
            elif vals[7] == "z":
                obj.pos[2] = float(vals[9])
        elif vals[5] == "rot":
            if vals[7] == "x":
                obj.rot[0] = float(vals[9])
            elif vals[7] == "y":
                obj.rot[1] = float(vals[9])
            elif vals[7] == "z":
                obj.rot[2] = float(vals[9])

    def parse_fallout_line(self, obj, vals):
        if vals[5] == "pos":
            if vals[7] == "y":
                obj.y = float(vals[9])

    def parse_animobj_line(self, obj, vals):
        if vals[5] == "center":
            if vals[7] == "x":
                obj.pos[0] = float(vals[9])
            elif vals[7] == "y":
                obj.pos[1] = float(vals[9])
            elif vals[7] == "z":
                obj.pos[2] = float(vals[9])
        elif vals[5] == "name":
            obj.name = vals[9]
        elif vals[5] == "file":
            obj.file = vals[9]

    def parse_animobjs(self):
        for k, v in self.animobj_map.items():
            fp = os.path.join(os.path.dirname(self.config_path), v.file)
            with open(fp, "r") as f:
                lines = f.readlines()

            for line in lines:
                vals = line.split()
                if len(vals) < 10:
                    if len(vals) > 0:
                        print(
                            f"!! Ignoring line \"{line[:-1]}\"",
                            file=sys.stderr
                            )
                    continue

                if vals[0] == "frame":
                    obj = v.keyframe_map.setdefault(vals[2], Keyframe())
                    self.parse_keyframe_line(obj, vals)
                else:
                    print(f"!! Ignoring line \"{line[:-1]}\"", file=sys.stderr)

    def parse_keyframe_line(self, obj, vals):
        if vals[5] == "pos":
            if vals[7] == "x":
                obj.pos[0] = float(vals[9])
            elif vals[7] == "y":
                obj.pos[1] = float(vals[9])
            elif vals[7] == "z":
                obj.pos[2] = float(vals[9])
        elif vals[5] == "rot":
            if vals[7] == "x":
                obj.rot[0] = float(vals[9])
            elif vals[7] == "y":
                obj.rot[1] = float(vals[9])
            elif vals[7] == "z":
                obj.rot[2] = float(vals[9])
        elif vals[5] == "time":
            obj.time = float(vals[9])

    def parse_bg_line(self, obj, vals):
        obj.name = vals[9]

    def package_config(self) -> Config:
        c = Config()

        c.goal_map = self.goal_map
        c.bumper_map = self.bumper_map
        c.banana_map = self.banana_map
        c.jamabar_map = self.jamabar_map
        c.start_map = self.start_map
        c.fallout_map = self.fallout_map
        c.animobj_map = self.animobj_map
        c.bg_map = self.bg_map

        return c
