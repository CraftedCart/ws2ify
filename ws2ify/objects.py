class Config:
    def __init__(self):
        self.goal_map = {}
        self.bumper_map = {}
        self.banana_map = {}
        self.jamabar_map = {}
        self.start_map = {}
        self.fallout_map = {}
        self.animobj_map = {}
        self.bg_map = {}


class Goal:
    def __init__(self):
        self.pos = [0.0, 0.0, 0.0]
        self.rot = [0.0, 0.0, 0.0]
        self.type = "B"


class Bumper:
    def __init__(self):
        self.pos = [0.0, 0.0, 0.0]
        self.rot = [0.0, 0.0, 0.0]
        self.scl = [0.0, 0.0, 0.0]


class Banana:
    def __init__(self):
        self.pos = [0.0, 0.0, 0.0]
        self.type = "N"


class Jamabar:
    def __init__(self):
        self.pos = [0.0, 0.0, 0.0]
        self.rot = [0.0, 0.0, 0.0]
        self.scl = [0.0, 0.0, 0.0]


class Start:
    def __init__(self):
        self.pos = [0.0, 0.0, 0.0]
        self.rot = [0.0, 0.0, 0.0]


class Fallout:
    def __init__(self):
        self.y = 0.0


class AnimObj:
    def __init__(self):
        self.pos = [0.0, 0.0, 0.0]
        self.name = ""
        self.file = ""
        self.keyframe_map = {}


class Bg:
    def __init__(self):
        self.name = ""


class Keyframe:
    def __init__(self):
        self.pos = [0.0, 0.0, 0.0]
        self.rot = [0.0, 0.0, 0.0]
        self.time = 0.0
