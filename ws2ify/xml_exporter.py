from xml.etree import ElementTree
from xml.etree.ElementTree import Element, SubElement
from xml.dom import minidom
import pathlib
import os


class XmlExporter:
    def __init__(self, cfg, obj):
        self.cfg = cfg
        self.obj = obj

    def export_to(self, file, kf_easing):
        root = Element("superMonkeyBallStage", {"version": "1.1.0"})

        SubElement(root, "modelImport").text = pathlib.Path(os.path.abspath(self.obj)).as_uri()

        # TODO: Start, Fallout
        start = SubElement(root, "start")
        SubElement(
            start,
            "position",
            {
                "x": str(self.cfg.start_map["0"].pos[0]),
                "y": str(self.cfg.start_map["0"].pos[1]),
                "z": str(self.cfg.start_map["0"].pos[2])
            }
            )

        SubElement(
            start,
            "rotation",
            {
                "x": str(self.cfg.start_map["0"].rot[0]),
                "y": str(self.cfg.start_map["0"].rot[1]),
                "z": str(self.cfg.start_map["0"].rot[2])
            }
            )

        SubElement(root, "falloutPlane", {"y": str(self.cfg.fallout_map["0"].y)})

        static_ig = self.create_item_group(root)
        self.create_goals(static_ig)
        self.create_bumpers(static_ig)
        self.create_bananas(static_ig)
        self.create_jamabars(static_ig)
        self.create_static_objects(static_ig)

        for obj in self.cfg.bg_map.values():
            e = SubElement(root, "backgroundModel")
            SubElement(e, "name").text = obj.name

        for obj in self.cfg.animobj_map.values():
            ig = self.create_item_group(root, obj.pos)

            e = SubElement(ig, "stageModel")
            SubElement(e, "name").text = obj.name
            c = SubElement(e, "collision")
            m = SubElement(c, "meshCollision")
            SubElement(m, "name").text = obj.name

            kf = SubElement(ig, "animKeyframes")
            kfpx = SubElement(kf, "posX")
            kfpy = SubElement(kf, "posY")
            kfpz = SubElement(kf, "posZ")
            kfrx = SubElement(kf, "rotX")
            kfry = SubElement(kf, "rotY")
            kfrz = SubElement(kf, "rotZ")

            for k in obj.keyframe_map.values():
                SubElement(
                    kfpx,
                    "keyframe",
                    {
                        "time": str(k.time),
                        "value": str(k.pos[0]),
                        "easing": kf_easing
                    }
                    )

                SubElement(
                    kfpy,
                    "keyframe",
                    {
                        "time": str(k.time),
                        "value": str(k.pos[1]),
                        "easing": kf_easing
                    }
                    )

                SubElement(
                    kfpz,
                    "keyframe",
                    {
                        "time": str(k.time),
                        "value": str(k.pos[2]),
                        "easing": kf_easing
                    }
                    )

                SubElement(
                    kfrx,
                    "keyframe",
                    {
                        "time": str(k.time),
                        "value": str(k.rot[0]),
                        "easing": kf_easing
                    }
                    )

                SubElement(
                    kfry,
                    "keyframe",
                    {
                        "time": str(k.time),
                        "value": str(k.rot[1]),
                        "easing": kf_easing
                    }
                    )

                SubElement(
                    kfrz,
                    "keyframe",
                    {
                        "time": str(k.time),
                        "value": str(k.rot[2]),
                        "easing": kf_easing
                    }
                    )

        contents = minidom.parseString(ElementTree.tostring(root)).toprettyxml(indent="    ")

        with open(file, "w") as f:
            f.write(contents)

    def create_item_group(self, root, pos=[0.0, 0.0, 0.0]):
        ig = SubElement(root, "itemGroup")
        collision_grid = SubElement(ig, "collisionGrid")
        SubElement(collision_grid, "start", {"x": "-256", "z": "-256"})
        SubElement(collision_grid, "step", {"x": "32", "z": "32"})
        SubElement(collision_grid, "count", {"x": "16", "z": "16"})

        SubElement(ig, "animSeesawType").text = "PLAY_ONCE_ANIMATION"
        SubElement(ig, "animGroupId").text = "0"
        SubElement(ig, "animInitialState").text = "PLAY"
        SubElement(ig, "rotationCenter", {"x": str(pos[0]), "y": str(pos[1]), "z": str(pos[2])})
        SubElement(ig, "initialRotation", {"x": "0", "y": "0", "z": "0"})

        return ig

    def create_goals(self, ig):
        for goal in self.cfg.goal_map.values():
            e = SubElement(ig, "goal")
            SubElement(
                e,
                "position",
                {
                    "x": str(goal.pos[0]),
                    "y": str(goal.pos[1]),
                    "z": str(goal.pos[2])
                }
                )

            SubElement(
                e,
                "rotation",
                {
                    "x": str(goal.rot[0]),
                    "y": str(goal.rot[1]),
                    "z": str(goal.rot[2])
                }
                )

            SubElement(e, "type").text = self.goal_type_to_enum(goal.type)

    def create_bumpers(self, ig):
        for bumper in self.cfg.bumper_map.values():
            e = SubElement(ig, "bumper")
            SubElement(
                e,
                "position",
                {
                    "x": str(bumper.pos[0]),
                    "y": str(bumper.pos[1]),
                    "z": str(bumper.pos[2])
                }
                )

            SubElement(
                e,
                "rotation",
                {
                    "x": str(bumper.rot[0]),
                    "y": str(bumper.rot[1]),
                    "z": str(bumper.rot[2])
                }
                )

            SubElement(
                e,
                "scale",
                {
                    "x": str(bumper.scl[0]),
                    "y": str(bumper.scl[1]),
                    "z": str(bumper.scl[2])
                }
                )

    def create_bananas(self, ig):
        for banana in self.cfg.banana_map.values():
            e = SubElement(ig, "banana")
            SubElement(
                e,
                "position",
                {
                    "x": str(banana.pos[0]),
                    "y": str(banana.pos[1]),
                    "z": str(banana.pos[2])
                }
                )

            SubElement(e, "type").text = self.banana_type_to_enum(banana.type)

    def create_jamabars(self, ig):
        for jamabar in self.cfg.jamabar_map.values():
            e = SubElement(ig, "jamabar")
            SubElement(
                e,
                "position",
                {
                    "x": str(jamabar.pos[0]),
                    "y": str(jamabar.pos[1]),
                    "z": str(jamabar.pos[2])
                }
                )

            SubElement(
                e,
                "rotation",
                {
                    "x": str(jamabar.rot[0]),
                    "y": str(jamabar.rot[1]),
                    "z": str(jamabar.rot[2])
                }
                )

            SubElement(
                e,
                "scale",
                {
                    "x": str(jamabar.scl[0]),
                    "y": str(jamabar.scl[1]),
                    "z": str(jamabar.scl[2])
                }
                )

    def create_static_objects(self, ig):
        with open(self.obj, "r") as f:
            lines = f.readlines()

        for line in lines:
            if line.startswith("o "):
                split = line.split()

                is_ignored = False
                for obj in self.cfg.animobj_map.values():
                    if obj.name == split[1]:
                        is_ignored = True
                        break

                if not is_ignored:
                    for obj in self.cfg.bg_map.values():
                        if obj.name == split[1]:
                            is_ignored = True
                            break

                if not is_ignored:
                    e = SubElement(ig, "stageModel")
                    SubElement(e, "name").text = split[1]
                    c = SubElement(e, "collision")
                    m = SubElement(c, "meshCollision")
                    SubElement(m, "name").text = split[1]

    def goal_type_to_enum(self, t):
        if t == "G":
            return "GREEN"
        elif t == "R":
            return "RED"
        else:
            return "BLUE"

    def banana_type_to_enum(self, t):
        if t == "B":
            return "BUNCH"
        else:
            return "SINGLE"
